package principal;

import java.util.Scanner;

import clases.Castillo;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Castillo castilloCamelot = new Castillo();
		
		int num = 0;
		
		Scanner input = new Scanner (System.in);
		
		do {
			
			System.out.println("Elige una de estas opciones:");
			
			System.out.println("1- Crear el castillo y se crea un rey");
			System.out.println("2- Dar de alta 3 caballeros reales");
			System.out.println("3- Listar caballeros reales");
			System.out.println("4- Buscar a un caballero real por su nombre");
			System.out.println("5- Eliminar un caballero real por su nombre y listar");
			System.out.println("6- Dar de altar 3 escuderos");
			System.out.println("7- Listar escuderos");
			System.out.println("8- Buscar a un escudero por su nombre");
			System.out.println("9- Eliminar a un escudero por su nombre y listar");
			System.out.println("10- Listar a un escudero por su reino");
			System.out.println("11- Salir del programa");
			
			num = input.nextInt();
			
			input.nextLine();
			
			switch(num) {
			
			case 1:
				
				castilloCamelot.altaRey("Arturo", "Pendragon", 58, 1.92, 88.7, "Inglaterra");
				System.out.println("Se ha creado el castillo de Camelot y su rey");
				
				break;
				
			case 2:
				
				castilloCamelot.altaCaballeroReal("Lancelot", "Del Lago", 45, 1.87, 83.0, "Espanna", 30, "espada larga");
				castilloCamelot.altaCaballeroReal("Percivale", "De Gales", 50, 1.82, 88.1, "Inglaterra", 35, "hacha de dos manos");
				castilloCamelot.altaCaballeroReal("Lamorak", "De Gales", 38, 2.02, 98.6, "Inglaterra", 23, "espada corta y escudo");
				
				System.out.println("Se han creado las dos caballeros reales del rey Arturo");
				
				break;
				
			case 3:
				
				castilloCamelot.listarCaballeroReal();
				
				break;
				
			case 4:
				
				System.out.println("Introduce el nombre de un caballero real");
				String nomReal = input.nextLine();
				
				castilloCamelot.buscarCabRealPorNombre(nomReal);
				
				break;
				
			case 5:
				
				System.out.println("Introduce el nombre de un caballero real que quieras eliminar");
				String nomRealBorrar = input.nextLine();
				
				castilloCamelot.borrarCabReal(nomRealBorrar);
				
				System.out.println();
				
				System.out.println("Lista de los caballeros reales que quedan");
				castilloCamelot.listarCaballeroReal();
				
				break;
				
			case 6:
				
				castilloCamelot.altaEscudero("Tristan", "De Leonis", 13, 1.43, 39.9, "Espanna", 3, "real");
				castilloCamelot.altaEscudero("Bors", "De Ganis", 14, 1.67, 71.0, "Francia", 4, "normal");
				castilloCamelot.altaEscudero("Gawain", "Lot", 11, 1.55, 42.9, "Francia", 1, "real");
				
				System.out.println("Se han creado los tres escuderos que un dia ser�n caballeros reales");
				
				break;
				
			case 7:
				
				castilloCamelot.listarEscudero();
				
				break;
				
			case 8:
				
				System.out.println("Introduce el nombre de un escudero");
				String nomEscu = input.nextLine();
				
				castilloCamelot.buscarEscuderoPorNombre(nomEscu);
				
				break;
				
			case 9:
				
				System.out.println("Introduce el nombre del escudero que quieres eliminar");
				String nomEscuBorrar = input.nextLine();
				
				castilloCamelot.borrarEscudero(nomEscuBorrar);
				
				System.out.println();
				
				System.out.println("Nombre de los escuderos que quedan");
				castilloCamelot.listarEscudero();
				
				break;
				
			case 10:
				
				System.out.println("Introduce un reino para listar los escuderos procedientes de el");
				String nomReino = input.nextLine();
				
				castilloCamelot.listarEscuderoPorReino(nomReino);
				
				break;
				
			case 11:
				
				System.out.println("La aplicacion ha terminado");
				
				break;
			
			default:
				
				System.out.println("La opcion que has escogido no existe");
				
				break;
			}
			
		}while(num != 11);
		
		input.close();
		
	}

}
