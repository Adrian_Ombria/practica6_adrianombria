package clases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Castillo {

	ArrayList<CaballeroReal> caballeroReal;
	ArrayList<Escudero> escudero;
	ArrayList<Rey> rey;
	
	Scanner input = new Scanner (System.in);
	
	public Castillo() {
		
		this.caballeroReal = new ArrayList<CaballeroReal>();
		this.escudero = new ArrayList<Escudero>();
		this.rey = new ArrayList<Rey>();
	}
	
	public void mostrarCastillo() {
		
		listarRey();
		listarCaballeroReal();
		listarEscudero();
	}
	
	/*Rey*/
	
	public void altaRey(String nombre, String apellido, int edad, double altura, double peso, String reino) {
		
		Rey nuevoRey = new Rey(nombre, apellido, edad, altura, peso, reino);
		rey.add(nuevoRey);
	}
	
	public void listarRey() {
		
		for(Rey mandamas : rey) {
			
			if(mandamas != null) {
				System.out.println(mandamas.toString());
			}
		}
	}
	
	/*Caballero real*/
	
	public void altaCaballeroReal(String nombre, String apellido, int edad, double altura, double peso, String reino, 
			int annosDeCaballero, String tipoArma) {
		
		if(!existeCabReal(nombre)) {
			CaballeroReal nuevoCabReal = new CaballeroReal(nombre, apellido, edad, altura, peso, reino, annosDeCaballero, tipoArma);
			caballeroReal.add(nuevoCabReal);
		}
		
	}
	
	public boolean existeCabReal(String nombre) {
		
		for(CaballeroReal cabReal : caballeroReal) {
			
			if(cabReal != null && cabReal.getNombre().equals(nombre)) {
				return true;
			}
		}
		
		return false;
	}
	
	public void listarCaballeroReal() {
		
		for(CaballeroReal cabReal : caballeroReal) {
			
			if(cabReal != null) {
				System.out.println(cabReal.toString());
			}
		}
	}
	
	public void buscarCabRealPorNombre(String nombre) {
		
		for(CaballeroReal cabReal : caballeroReal) {
			
			if(cabReal.getNombre().equals(nombre)) {
				System.out.println("El caballero con ese nombre es: " + cabReal.toString());
			}
		}
	}
	
	public void borrarCabReal(String nombre) {
		
		Iterator<CaballeroReal> iteratorCabReal = caballeroReal.iterator();
		
		while(iteratorCabReal.hasNext()) {
			CaballeroReal caballeroReal = iteratorCabReal.next();
			
			if(caballeroReal.getNombre().equals(nombre)) {
				iteratorCabReal.remove();
			}
		}
	}
	
	/*Escudero*/
	
	public void altaEscudero(String nombre, String apellido, int edad, double altura, double peso, String reino, 
			int annosDeEscudero, String sangre) {
		
		if(!existeEscudero(nombre)) {
			Escudero nuevoEscudero = new Escudero(nombre, apellido, edad, altura, peso, reino, annosDeEscudero, sangre);
			escudero.add(nuevoEscudero);
		}
		
	}
	
	public boolean existeEscudero(String nombre) {
		
		for(Escudero escu : escudero) {
			
			if(escu != null && escu.getNombre().equals(nombre)) {
				return true;
			}
		}
		
		return false;
	}
	
	public void listarEscudero() {
		
		for(Escudero escu : escudero) {
			
			if(escu != null) {
				System.out.println(escu.toString());
			}
		}
	}
	
	public ArrayList<Escudero> buscarEscuderoPorNombre(String nombre){
		
		for(Escudero escu : escudero) {
			
			if(escu.getNombre().equals(nombre)) {
				System.out.println("El escudero con ese nombre es: " + escu.toString());
				
				return escudero;
			}
		}
		
		return null;
	}
	
	public void borrarEscudero(String nombre) {
		
		Iterator<Escudero> iteratorEscudero = escudero.iterator();
		
		while(iteratorEscudero.hasNext()) {
			Escudero escudero = iteratorEscudero.next();
			
			if(escudero.getNombre().equals(nombre)) {
				iteratorEscudero.remove();
			}
		}
	}
	
	public void listarEscuderoPorReino(String reino) {
		
		for(Escudero escu : escudero) {
			
			if(escu != null) {
				
				if(escu.getReino() == reino) {
					System.out.println(escu.toString());
				}
			}
		}
	}
}
