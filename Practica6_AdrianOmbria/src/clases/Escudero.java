package clases;

public class Escudero extends Caballero{

	private int annosDeEscudero;
	private String sangre;
	
	public Escudero(String nombre, String apellido, int edad, double altura, double peso, String reino, 
			int annosDeEscudero, String sangre) {
		
		super(nombre, apellido, edad, altura, peso, reino);
		this.annosDeEscudero = annosDeEscudero;
		this.sangre = sangre;
	}
	
	public String getSangre() {
		return sangre;
	}
	public void setSangre(String sangre) {
		this.sangre = sangre;
	}
	public int getAnnosDeEscudero() {
		return annosDeEscudero;
	}
	public void setAnnosDeEscudero(int annosDeEscudero) {
		this.annosDeEscudero = annosDeEscudero;
	}
	
	public String toString() {
		
		return super.toString() + ", " + annosDeEscudero + " annos de escudero, " + " sangre " + sangre;
	}
	
}
