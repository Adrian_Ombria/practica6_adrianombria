package clases;

public class Caballero {

	private String nombre;
	private String apellido;
	private int edad;
	private double altura;
	private double peso;
	private String reino;
	
	public Caballero(String nombre, String apellido, int edad, double altura, double peso, String reino) {
		
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.altura = altura;
		this.peso = peso;
		this.reino = reino;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getReino() {
		return reino;
	}
	public void setReino(String reino) {
		this.reino = reino;
	}
	
	public String toString() {
		
		return nombre + ", " + apellido + ", " + edad + " annos, " + altura + " m, " + peso + " kg, " + reino;
	}
}
