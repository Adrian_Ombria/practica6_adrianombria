package clases;

public class CaballeroReal extends Caballero{


	private int annosDeCaballero;
	private String tipoArma;
	
	public CaballeroReal(String nombre, String apellido, int edad, double altura, double peso, String reino, 
			int annosDeCaballero, String tipoArma) {
		
		super(nombre, apellido, edad, altura, peso, reino);
		this.annosDeCaballero = annosDeCaballero;
		this.tipoArma = tipoArma;
	}
	
	public int getAnnosDeCaballero() {
		return annosDeCaballero;
	}
	public void setAnnosDeCaballero(int annosDeCaballero) {
		this.annosDeCaballero = annosDeCaballero;
	}
	public String getTipoArma() {
		return tipoArma;
	}
	public void setTipoArma(String tipoArma) {
		this.tipoArma = tipoArma;
	}
	
	public String toString() {
		
		return super.toString() + ", " + annosDeCaballero + " annos de caballero real, " + " utiliza: " + tipoArma;
	}
	
}

