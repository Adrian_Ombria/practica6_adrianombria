package clases;

import java.util.ArrayList;

public class Rey extends Caballero{

int numReinosQueGobierna;
	
	ArrayList<String> reinoQueGobierna = new ArrayList<String>();
	
	public Rey(String nombre, String apellido, int edad, double altura, double peso, String reino) {
		
		super(nombre, apellido, edad, altura, peso, reino);
	}

	public int getNumReinosQueGobierna() {
		return numReinosQueGobierna;
	}

	public void setNumReinosQueGobierna(int numReinosQueGobierna) {
		this.numReinosQueGobierna = numReinosQueGobierna;
	}

	public ArrayList<String> getReinoQueGobierna() {
		return reinoQueGobierna;
	}

	public void setReinoQueGobierna(ArrayList<String> reinoQueGobierna) {
		this.reinoQueGobierna = reinoQueGobierna;
	}
	
	public String toString() {
		
		return super.toString() + numReinosQueGobierna + " persona que gobierna, " + reinoQueGobierna;
	}

}
